SQLAlchemy field processing module


Using:

    import pilkit
    from brpr.sqla.fieldprocessor import field_processor
    from brpr.sqla.fieldprocessor.processors import AttachmentImageProcessor

    @field_processor
    class Test(db.Model):
        __tablename__ = '...'
        text = db.Column(db.String)
        image = db.relationship(Attachment, ...)

        ...

        __field_process_ = dict(
            image = AttachmentImageProcessor(pilkit.ResizeToFit, width=550, height=550),
        )


1. field may be a list of processors in __field_process_ than applaying some set of processors
2. any processor must inherit BaseProcessor class and implement self __init__() and process() methods
3. if field in __field_process__ must apply some set of processors, it may be a list of Processor's object
