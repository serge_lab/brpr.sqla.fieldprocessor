
from flask import current_app
from flask.ext.script import Command, Option


class Reprocess(Command):
    description = 'Reprocessing all fields with fieldprocessor'

    def __init__(self, session):
        self.session = session

    def run(self):
        from . import FieldManager
        FieldManager.reprocess(self.session)
