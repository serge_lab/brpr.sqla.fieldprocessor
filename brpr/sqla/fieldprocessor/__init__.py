from sqlalchemy.orm.attributes import get_history
from abc import ABCMeta, abstractmethod


class BaseProcessor(object):
    __metaclass__ = ABCMeta

    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def process(self, data):
        pass


class FieldManager(object):
    _hooks = []

    def __init__(self, cls):
        self.processes = getattr(cls, '__field_process__', [])
        if not cls in self._hooks:
            self._hooks.append(cls)

    def _check_history(self, attr, instance, check_history=True):
        if not check_history:
            return True
        old_val = get_history(instance, attr)
        if old_val.added:
            return True
        return False

    def _processing(self, instance, check_history=True):
        for field, process in self.processes.iteritems():
            if isinstance(process, list):
                for proc in process:
                    if hasattr(instance, field) and self._check_history(field, instance, check_history):  # Check if object attribute was changed
                        if isinstance(getattr(instance, field), list):
                            for d in getattr(instance, field):
                                proc.process(d)
                        else:
                            proc.process(getattr(instance, field))
            else:
                if hasattr(instance, field) and self._check_history(field, instance, check_history):  # Check if object attribute was changed
                    if isinstance(getattr(instance, field), list):
                        for d in getattr(instance, field):
                            process.process(d)
                    else:
                        process.process(getattr(instance, field))

    def after_update(self, mapper, connection, instance):
        self._processing(instance)

    def after_insert(self, mapper, connection, instance):
        self._processing(instance, False)

    @classmethod
    def reprocess(cls, session, model=None):
        for c in cls._hooks:
            fm_manager = cls(c)
            for inst in session.query(c).all():
                fm_manager._processing(inst, False)


def field_processor(cls):
    if hasattr(cls, '__field_process__'):
        from sqlalchemy import event
        fm_manager = FieldManager(cls)
        event.listen(cls.__mapper__, 'after_update', fm_manager.after_update, propagate=False)
        event.listen(cls.__mapper__, 'after_insert', fm_manager.after_insert, propagate=False)

    return cls
