from __future__ import absolute_import
from setuptools import setup, find_packages

# It's a fast fix for http://hg.python.org/cpython/rev/0a58fa8e9bac
try:
    import multiprocessing
except ImportError:
    pass


from distutils.command.clean import clean as CleanCommand

import os
import shutil
import sys
import fnmatch


def findNremove(path, pattern, maxdepth=1):
    cpath=path.count(os.sep)
    for r,d,f in os.walk(path):
        if maxdepth == 0 or r.count(os.sep) - cpath < maxdepth:
            for files in f:
                if files.endswith(pattern):
                    try:
                        os.remove(os.path.join(r, files))
                    except Exception as e:
                        print(e)


class clean(CleanCommand):
    def run(self):
        shutil.rmtree(self.distribution.metadata.name + '.egg-info', True)
        shutil.rmtree('dist', True)
        shutil.rmtree('build', True)
        findNremove('.', '.pyc', 0)
        CleanCommand.run(self)


def find_package_data(package, src='', filter='*'):
    matches = []
    where = package.replace('.', os.sep)
    for root, dirnames, filenames in os.walk(os.path.join(where, src)):
        for filename in fnmatch.filter(filenames, filter):
          matches.append( os.path.relpath(os.path.join(root, filename), where) )

    return { package : matches }


if __name__ == '__main__':
    setup(name='brpr.sqla.fieldprocessor',
          version='0.2',
          author='Sergey Syrov',
          author_email='serge@brpr.ru',
          url='http://pypi.tronet.ru/simple/brpr.sqla.fieldprocessor/',
          description='SQLAlchemy field processor',
          long_description=open('README.md').read(),
          namespace_packages=['brpr', 'brpr.sqla'],
          tests_require=['nose'],
          extras_require={'test': ['nose']},
          packages=find_packages('.'),
          package_dir={'': '.'},
          include_package_data=True,
          package_data=find_package_data('brpr.sqla.fieldprocessor'),
          cmdclass={'clean': clean},
          test_suite='nose.collector',
          zip_safe=False)
